Overview
--------

The CSUconnect Federation provides an attribute filter definition for Shibboleth Identity Providers that releases 
the required attributes for CSUconnect Federation applications. The configuration files and instructions will 
be updated and made available to you as needed.

Included Files
--------------

00readme.txt : this file
csuconnect-attribute-filter.xml : an attribute filter configuration file for csuconnect service providers 
    used by all campuses.
csuconnect-optional-attribute-filter.xml : an attribute filter configuration file for optional services.
csuconnect.properties : a mapping file to connect your IdP attributes to the csuconnect-attribute-filter.xml 
    and csuconnect-optional-attribute-filter.xml.

Instructions
------------

1. Place a copy of the csuconnect-attribute-filter.xml and csuconnect.properties files in %{idp.home}/conf. If you are using
the csuconnect-optional-attribute-filter.xml, place a copy of that file in the same directory as well.
   
2. Open the csuconnect.properties files and set each attribute property definition to the id value from your attribute
   resolver (attribute-resolver.xml) for the corresponding attribute. For example, if your attribute resolver has the 
   following definition:
   
     <AttributeDefinition id="surname" xsi:type="Simple"
         sourceAttributeID="sn">
         <Dependency ref="myLDAP" />
         <AttributeEncoder name="urn:oid:2.5.4.4" friendlyName="sn" encodeType="false" xsi:type="SAML2String" />
     </AttributeDefinition>

   since id="surname", then the value of edu.calstate.csuconnect.attribute.sn.attributeID should be set to surname:
   
   edu.calstate.csuconnect.attribute.sn.attributeID=surname

3. In %{idp.home}/conf/idp.properties, add the following to idp.additionalProperties:

   , /conf/csuconnect.properties
   
4. In %{idp.home}/conf/services.xml add an additional value element with the value %{idp.home}/conf/csuconnect-attribute-filter.xml
   to shibboleth.AttributeFilterResources:
   
    <util:list id ="shibboleth.AttributeFilterResources">
        <value>%{idp.home}/conf/attribute-filter.xml</value>
        <value>%{idp.home}/conf/csuconnect-attribute-filter.xml</value>
    </util:list>

    If you want to use the optional attribute filter as well add an extra value element for it as well:

    <util:list id ="shibboleth.AttributeFilterResources">
        <value>%{idp.home}/conf/attribute-filter.xml</value>
        <value>%{idp.home}/conf/csuconnect-attribute-filter.xml</value>
        <value>%{idp.home}/conf/csuconnect-optional-attribute-filter.xml</value>
    </util:list>

5. Restart your servlet container for the IdP, and check your logs for any errors.

6. Check your %{idp.home}/conf/attribute-filter.xml file for release policies for the identical services in the 
   csuconnect-attribute-filter.xml and csuconnect-optional-attribute-filter.xml files and comment them out or remove them from 
   attribute-filter.xml.